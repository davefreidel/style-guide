;(function($, window, undefined) {
    var $body, $nav;

    $(function() {
        $body = $('body');
        $nav = $("#nav");
        $body.on("click", "[data-menu-trigger]", toggleMenu);
    })

    function toggleMenu() {
        if ($nav.hasClass('active')) {
            $nav.removeClass('active');
        } else {
            $nav.addClass('active');
        }
    }
})(jQuery, window);